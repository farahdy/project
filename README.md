<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


## Nama Kelompok

Kami dari kelompok 7. 
Disini kami membuat suatu website untuk perpustakaan dimana dapat melihat list buku dan meminjam serta mengembalikan. Nah user akan diminta untuk membuat akun supaya dapat login dan dapat mengganti profile sesuai yang diinginkan.
1. Dyah Ayu Farah Anggraeni (menangani profile, erd, crud profile, menambahkan library package seperti pdf dan excel namun tidak dapat berjalan, dan telah mencoba melakukan deploy namun ada kendala di postgresql dimana data dari database local saya tidak bisa migrate di postgresql)
2. M. Farhan Kisnanda (menangani users, erd, crud users, dan telah mencoba untuk melakukan deploy)
3. Dus Jati Kris Hendhita (menangani buku, namun sepertinya ada kendala sehingga tidak berprogress)

## Link Video

1. Link Video Demonstrasi Users:
2. Link Video Demonstrasi Profiles: https://intip.in/demoProfilesByFarah
3. Link Video Demonstrasi Buku: 

## Link Deploy

https://blooming-island-99933.herokuapp.com
--> namun sangat disayangkan postgresql nya terdapat masalah dan tidak bisa digunakan dengan baik. Untuk itu, kami tidak bisa melanjutkan deploy website. 
