<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;
use Auth;
use App\Exports\ProfilesExport;
use Maatwebsite\Excel\Facades\Excel;

class ProfileController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->only('create', 'edit', 'update', 'store');
    }
    public function lihat() {
        $profile = DB::table('profiles')->get();
        //$Profile = Profile::all(); 

        return view('profile.index', compact('profile'));
    }
    public function index() {
        //$profile = DB::table('profiles')->get();
        $profile = Profile::all(); 

        return view('profile.index', compact('profile'));
    }

    public function create() {
        return view('profile.create');
    }   
    
    public function store(Request $request) {
        //dd($request->all());
        $request -> validate([
            'nama_lengkap' => 'required|unique:profiles',
            'email' => 'required',
            'kontak' => 'required'
        ]);
        $query = DB::table('profiles')->insert([
            "nama_lengkap" => $request["nama_lengkap"],
            "email" => $request["email"],
            "kontak" => $request["kontak"],
            "foto" => $request["foto"],
            "user_id" => Auth::id()
        ]);

        // $Profile = new Profile;
        // $profile->nama_lengkap = $request["nama_lengkap"];
        // $profile->email = $request["email"];
        // $profile->kontak = $request["kontak"];
        // $profile->foto = $request["foto"];
        // $profile->save(); //insert into profile

        // $profile = Profile::create([
        //     "nama_lengkap" => $request["nama_lengkap"],
        //     "email" => $request["email"],
        //     "kontak" => $request["kontak"],
        //     "foto" => $request["foto"],

        // ]);

        return redirect("/profile/create")->with('success', 'Profile Berhasil Disimpan');
    }

    public function show($profile_id) {
        $profile = DB::table('profiles')
                            ->where('id', $profile_id)
                            ->first();
        //$profile = Profile::find($profile_id);
        return view('profile.profile', compact('profile'));
    }

    public function edit($profile_id) {
        $profile= DB::table('profiles')
                            ->where('id', $profile_id)
                            ->first();
        //$profile = Profile::find($profile_id);
        return view('profile.edit', compact('profile'));
    }

    public function update($profile_id, Request $request) {
        $request -> validate([
            'nama_lengkap' => 'required|unique:profiles',
            'email' => 'required',
            'kontak' => 'required'
        ]);
        
        // $query = DB::table('profiles')
        //         -> where('id', $profile_id)
        //         ->update([
        //             "nama_lengkap" => $request["nama_lengkap"],
        //             "email" => $request["email"],
        //             "kontak" => $request["kontak"],
        //             "foto" => $request["foto"]
        // ]);

        $update = Profile::where('id', $profile_id)->update([
            "nama_lengkap" => $request["nama_lengkap"],
            "email" => $request["email"],
            "kontak" => $request["kontak"],
            "foto" => $request["foto"]
        ]);
        return redirect("/profile")->with('success', 'Berhasil diperbarui');
    }

    public function destroy($profile_id) {
        $query = DB::table('profiles')
                -> where('id', $profile_id)
                ->delete();

        //Profile::destroy($id);
        return redirecting("/profile")->with('success', 'Berhasil dihapus');
    }
    public function export() 
    {
        return Excel::download(new ProfilesExport, 'profiles.xlsx');
    }
}
