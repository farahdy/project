<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    public function index(){
        $users = DB::table('users')->get();
        // dd($user);
        return view('users.index', compact('users'));
    }

    public function show($id) {
        $user = DB::table('users')->where('id', $id)->first();
        // dd($user);
        return view('users.show', compact('user'));
    }

    public function edit($id) {
        $user = DB::table('users')->where('id', $id)->first();
        return view('users.edit', compact('user'));
    }
    
    public function update($id, Request $request) { 
       $query = DB::table('users')
       ->where('id',$id)
       ->update([
           'name' => $request['name'],
           'email' => $request['email'],
           'password' => $request['password']
       ]);
        
        return redirect('/users')->with('success', 'Berhasil Update Post');
    }
    
    public function destroy($id) {
        $query = DB::table('users')->where('id', $id)->delete();
        return redirect('/users')->with('success', 'Post Berhasil Dihapus');
    }
    public function export() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
} 
