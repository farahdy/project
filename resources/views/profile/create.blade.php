@extends('master')

@section('content')
<section class="wrapper">
    <h3><i class="fa fa-angle-right"></i> Update Profile</h3>
    <div class="row mt">
            <div class="col-lg-12">
                <h4><i class="fa fa-angle-right"></i> Fill the Blank</h4>
                <div class="form-panel">
                <form role="form" class="form-horizontal style-form" action="/profile" method="POST">
                    @csrf
                    <div class="form-group">
                    <label class="col-lg-2 control-label">Full Name</label>
                    <div class="col-lg-10">
                        <input type="text" placeholder="Enter Name" id="nama_lengkap" name="nama_lengkap" value="{{ old('nama_lengkap')}}" class="form-control" required>
                        @error('nama_lengkap')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    </div>
                    <!-- <div class="form-group">
                    <label class="col-lg-2 control-label">Last Name</label>
                    <div class="col-lg-10">
                        <input type="text" placeholder="" id="lname" name="lname" class="form-control">
                    </div>
                    </div> -->
                    <div class="form-group">
                    <label class="col-lg-2 control-label">Email</label>
                    <div class="col-lg-10">
                        <input type="email" placeholder="Enter Email" id="email" name="email" value="{{ old('email')}}" class="form-control" required>
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-lg-2 control-label">Contact</label>
                    <div class="col-lg-10">
                        <input type="contact" placeholder="Enter Contact" id="kontak" name="kontak" value="{{ old('kontak')}}" class="form-control" required>
                        @error('kontak')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label"> Avatar</label>
                            <div class="col-lg-6">
                                <input type="file" id="exampleInputFile" id="foto" name="foto" class="file-pos">
                            </div>
                    </div>
                    <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button class="btn btn-theme" type="submit"><a href="/profile">Submit</a></button>
                    </div>
                    </div>
                </form>
                </div>
                <!-- /form-panel -->
            </div>
            <!-- /col-lg-12 -->
            </div>
</section>
@endsection

<!-- @push('scripts')
<script src="admin/lib/jquery/jquery.min.js"></script>
  <script src="admin/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="admin/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="admin/lib/jquery.scrollTo.min.js"></script>
  <script src="admin/lib/jquery.nicescroll.js" type="text/javascript"></script>
  common script for all pages-->
  <!-- <script src="admin/lib/common-scripts.js"></script>
  script for this page
  <script src="admin/lib/form-validation-script.js"></script> -->

<!--@endpush -->
