@extends('master')

@section('content')
<section class="wrapper">
    <h3><i class="fa fa-angle-right"></i>Edit Profile</h3>
    <div class="row mt">
        <div class="col-lg-12">
            <!-- <h4><i class="fa fa-angle-right"></i> Fill the Blank</h4> -->
            <div class="col-lg-12">
                <h4><i class="fa fa-angle-right"></i>Silakan mengisi form di bawah ini!</h4>
                <div class="form-panel">
                    <div class=" form">
                        <form class="cmxform form-horizontal style-form" id="commentForm" method="POST"
                            action="/profile/{{$profile->id}}">
                            @csrf
                            @method('PUT')
                            <div class="form-group ">
                                <label for="name" class="control-label col-lg-2">Nama Lengkap</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="name" name="nama_lengkap"
                                        value="{{old('nama_lengkap',$profile->nama_lengkap)}}" placeholder="Enter name" minlength="2"
                                        type="text" required="">
                                    @error('nama_lengkap')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="email" class="control-label col-lg-2">Email</label>
                                <div class="col-lg-10">
                                    <input class="form-control " id="email" type="email" name="email"
                                        value="{{old('email',$profile->email)}}" placeholder="Enter email" required="">
                                    @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="kontak" class="control-label col-lg-2">Kontak</label>
                                <div class="col-lg-10">
                                    <input class="form-control " id="kontak" type="kontak" name="kontak"
                                        value="{{old('kontak',$profile->kontak)}}" placeholder="Enter kontak">
                                    @error('kontak')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label"> Avatar</label>
                                    <div class="col-lg-6">
                                        <input type="file" id="exampleInputFile" id="foto" name="foto" class="file-pos">
                                    </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-theme" type="submit"><a href="/profile">Save</a></button>
                                    <button class="btn btn-theme04" type="button"><a href="/lihat/profile">Cancel</a></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /form-panel -->
            </div>
            <!-- /form-panel -->
        </div>
        <!-- /col-lg-12 -->
    </div>
</section>
@endsection