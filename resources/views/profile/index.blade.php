@extends('master')

@section('content')
<section class="wrapper">
    <h3><i class="fa fa-angle-right"></i>Update Profile Table</h3>
    <div class="row mt">
        <div class="col-lg-12">
            <!-- <h4><i class="fa fa-angle-right"></i> Fill the Blank</h4> -->

            <div class="row">
                @if(session('success'))
                <div class="alert alert-default-info">{{session('success')}}</div>
                @endif
                <div class="col-md-12">
                    <div class="content-panel">
                        <h4><i class="fa fa-angle-right"></i> Tabel Data Pembaruan Profile</h4>
                        <hr>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Lengkap</th>
                                    <th>Email</th>
                                    <th>Kontak</th>
                                    <th>Foto</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($profile as $key => $profile)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$profile->nama_lengkap}}</td>
                                    <td>{{$profile->email}}</td>
                                    <td>{{$profile->kontak}}</td>
                                    <td>{{$profile->foto}}</td>
                                    <td style="display:flex">
                                        <a href="/profile/{{$profile->id}}" class="btn-primary btn-sm">Show</a>
                                        <a href="/profile/{{$profile->id}}/edit" class="btn-default btn-sm">Edit</a>
                                        <form action="/profile/{{$profile->id}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <a href="/profile/{{$profile->id}}" class="btn btn-primary">Back to Home</a>
                    <button class="btn btn-theme" type="submit"><a href="/profile/create">Update Profile</a></button>
                    <button class="btn btn-default" type="submit"><a href="/profile/export">Print Excel</a></button>
                 </div>
            </div>
            <!-- /form-panel -->

        </div class="mt-3">
        <!-- /col-lg-12 -->


    </div>
</section>

@endsection