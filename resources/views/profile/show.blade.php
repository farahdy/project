@extends('master')

@section('content')
<section class="wrapper">
    <h3><i class="fa fa-angle-right"></i>Profile Table</h3>
    <div class="row mt">
        <div class="col-lg-12">
            <!-- <h4><i class="fa fa-angle-right"></i> Fill the Blank</h4> -->
            <h4>{{$profile->nama_lengkap}}</h4>
            <p>{{$profile->email}}</p>
            <p>{{$profile->kontak}}</p>
            <!-- /form-panel -->
        </div>
        <!-- /col-lg-12 -->
    </div>
</section>
@endsection