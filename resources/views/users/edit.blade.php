@extends('master')

@section('content')
<section class="wrapper">
    <h3><i class="fa fa-angle-right"></i>Users Table</h3>
    <div class="row mt">
        <div class="col-lg-12">
            <!-- <h4><i class="fa fa-angle-right"></i> Fill the Blank</h4> -->
            <div class="col-lg-12">
                <h4><i class="fa fa-angle-right"></i> Form Validations</h4>
                <div class="form-panel">
                    <div class=" form">
                        <form class="cmxform form-horizontal style-form" id="commentForm" method="POST"
                            action="/users/{{$user->id}}">
                            @csrf
                            @method('PUT')
                            <div class="form-group ">
                                <label for="name" class="control-label col-lg-2">Name</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="name" name="name"
                                        value="{{old('name',$user->name)}}" placeholder="Enter name" minlength="2"
                                        type="text" required="">
                                    @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="email" class="control-label col-lg-2">Email</label>
                                <div class="col-lg-10">
                                    <input class="form-control " id="email" type="email" name="email"
                                        value="{{old('email',$user->email)}}" placeholder="Enter email" required="">
                                    @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="password" class="control-label col-lg-2">Password</label>
                                <div class="col-lg-10">
                                    <input class="form-control " id="password" type="password" name="password"
                                        value="{{old('password',$user->password)}}" placeholder="Enter password">
                                    @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-theme" type="submit">Save</button>
                                    <button class="btn btn-theme04" type="button">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /form-panel -->
            </div>
            <!-- /form-panel -->
        </div>
        <!-- /col-lg-12 -->
    </div>
</section>
@endsection