@extends('master')

@section('content')
<section class="wrapper">
    <h3><i class="fa fa-angle-right"></i>Users Table</h3>
    <div class="row mt">
        <div class="col-lg-12">
            <!-- <h4><i class="fa fa-angle-right"></i> Fill the Blank</h4> -->

            <div class="row">
                @if(session('success'))
                <div class="alert alert-default-info">{{session('success')}}</div>
                @endif
                <div class="col-md-12">
                    <div class="content-panel">
                        <h4><i class="fa fa-angle-right"></i> Basic Table</h4>
                        <hr>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Password</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $key => $user)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->password}}</td>
                                    <td style="display:flex">
                                        <a href="/users/{{$user->id}}" class="btn-primary btn-sm">Show</a>
                                        <a href="/users/{{$user->id}}/edit" class="btn-default btn-sm">Edit</a>
                                        <form action="/users/{{$user->id}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <a href="/home" class="btn-primary btn-sm mt-3">Back to Home</a>
                    <a href="/users/export" class="btn-theme btn-sm mt-3">Print Excel</a>
                </div>
            </div>
            <!-- /form-panel -->

        </div class="mt-3">
        <!-- /col-lg-12 -->


    </div>
</section>
@endsection