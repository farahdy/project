<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/welcome', function () {
    return view('layouts.content');
});

Route::get('/coba', function () {
    return view('profile.profile');
});


Route::get('/lihat/profile', 'ProfileController@lihat');
Route::get('/profile/create', 'ProfileController@create');
Route::post('/profile', 'ProfileController@store');
Route::get('/profile', 'ProfileController@index');
Route::get('/profile/{id}', 'ProfileController@show');
Route::get('/profile/{id}/edit', 'ProfileController@edit');
Route::put('/profile/{id}', 'ProfileController@update');
Route::delete('/profile/{id}', 'ProfileController@destroy');
Route::get('/profile/export/', 'ProfileController@export');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', 'UserController@index');
Route::get('/users/{id}', 'UserController@show');
Route::get('/users/{id}/edit', 'UserController@edit');
Route::put('/users/{id}', 'UserController@update');
Route::delete('/users/{id}', 'UserController@destroy');
Route::get('/users/export/', 'UserController@export');

Route::get('/test-dompdf', function() {
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Hello World!</h1>');
    return $pdf->stream();
});

Route::get('/pdf', 'PdfController@test');